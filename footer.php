<?php

$site = new Timber\Site();

$context = array(
	'site' => $site,
	'general' => get_field('general', 'options')
);

Timber::render( 'footer.twig', $context );

wp_footer(); ?>
</div><!-- Close off-canvas wrapper inner -->
</div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->

</body>
</html>
