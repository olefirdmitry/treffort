<?php

// Gutenberg Only for ....
add_filter( 'use_block_editor_for_post_type', function( $enabled, $post_type ) {
	return in_array( $post_type, [ 'post' ] );
}, 10, 2 );
