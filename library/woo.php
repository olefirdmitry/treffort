<?php

add_filter('woocommerce_enqueue_styles', '__return_false');

function theme_add_woocommerce_support()
{
	add_theme_support('woocommerce');
}

function timber_set_product( $post ) {
	global $product;

	if ( is_woocommerce() ) {
		$product = wc_get_product( $post->ID );
	}
}

add_action('after_setup_theme', 'theme_add_woocommerce_support');

remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);

remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

function edit_woocommerce_catalog_orderby( $orderby ) {
	$orderby["date"] = __('Latest', 'woocommerce');
	$orderby["popularity"] = __('Popularity', 'woocommerce');
	$orderby["menu_order"] = __('All', 'woocommerce');
	$orderby["rating"] = __('Rating', 'woocommerce');
	$orderby["price"] = __('Price: Low to High', 'woocommerce');
	$orderby["price-desc"] = __('Price: High to Low', 'woocommerce');
	return $orderby;
}
add_filter( "woocommerce_catalog_orderby", "edit_woocommerce_catalog_orderby", 20 );

function edit_wocommerce_summary() {
	do_action('woocommerce_after_single_product_summary');
};
add_action( 'woocommerce_single_product_summary', 'edit_wocommerce_summary', 15 );

remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

function woocommerce_custom_single_add_to_cart_text() {
	return __( 'Add to Bag', 'woocommerce' );
}

add_filter( 'woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text' );

//add_theme_support( 'woocommerce', array(
//	'thumbnail_image_width' => 200,
//) );

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
