<?php
/*
Template Name: Front
*/
use Timber\Timber;

get_header(); ?>

<?php

$context = array(
	'hero' => get_field('hero'),
	'member' => get_field('member'),
	'look' => get_field('lookbook'),
	'features' => get_field('features'),
	'usp' => get_field('usp', 'options'),
	'cta' => get_field('cta', 'options')
);


Timber::render( 'templates/front.twig' , $context );
?>


<?php get_footer();
