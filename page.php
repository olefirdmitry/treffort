<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php
$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
Timber::render( 'page.twig' , $context );
?>

<?php
get_footer();
