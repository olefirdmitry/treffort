<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php
$context = Timber::get_context();
$post = new TimberPost();

$context['post'] = $post;

Timber::render( 'single.twig' , $context );

?>
<?php get_footer();
