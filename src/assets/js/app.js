import $ from 'jquery';

import './lib/_slick';

import './components/_header';

import whatInput from 'what-input';

window.$ = $;

// import Foundation from 'foundation-sites';

// If you want to pick and choose which modules to include, comment out the above and uncomment
// the line below
import './lib/foundation-explicit-pieces';

$(document).foundation();

$('[data-features-slider]').slick({
  centerPadding: '60px',
  infinite: false,
  slidesToShow: 1,
  slidesToScroll: 1,
  initialSlide: 1,
  speed: 1500,
  draggable: false,
});

document.addEventListener('DOMContentLoaded', () => {
  const cart = document.querySelector('.xoo-wsc-modal'),
    count = document.querySelector('.xoo-wsc-items-count'),
    cartWrap = document.querySelector('[data-cart-wrap]');

  cartWrap.addEventListener('click', () => {
    cart.classList.add('xoo-wsc-cart-active');
  });

  $('[data-product-sliderae]').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true
  });

}, false);

const zooms = document.querySelectorAll('[data-zoom]');

const resetZooms = () => {
  for (let i = 0; i < zooms.length; i++) {
    zooms[i].classList.remove('active');
  }
}


  for (let i = 0; i < zooms.length; i++) {
    const btn = zooms[i].querySelector('[data-zoom-btn]');

    btn.addEventListener('click', () => {
      // resetZooms();
      if (zooms[i].classList.contains('active')) {
        zooms[i].classList.remove('active');
      } else {
        zooms[i].classList.add('active');
      }
    })
  }

