// const menu = document.querySelector('[data-mob-menu]'),
//   offcanvas = document.querySelector('[data-offcanvas]'),
//   body = document.querySelector('body');
//
// menu.addEventListener('click', () => {
//   menu.classList.toggle('opened');
//   body.classList.toggle('menu-opened');
//   offcanvas.classList.toggle('opened');
// });

document.addEventListener('DOMContentLoaded', () => {
  const cart = document.querySelector('.xoo-wsc-modal');

  let curScroll;
  let prevScroll = window.scrollY || document.scrollTop;
  let curDirection = 0;
  let prevDirection = 0;

  let header = document.querySelector('[data-off-canvas-wrapper]');
  let toggled;
  let threshold = 50;

  let navOpened = false;
  const nav = document.querySelector('[data-offcanvas]');

  const checkScroll = function() {
    curScroll = window.scrollY || document.scrollTop;

    if (nav.classList.contains('opened')) {
      navOpened = true;
    } else {
      navOpened = false;
    }

    if(curScroll > prevScroll) {
      curDirection = 2;
    }
    else {
      curDirection = 1;
    }

    if(curDirection !== prevDirection && !navOpened) {
      toggled = toggleHeader();
    }

    prevScroll = curScroll;

    if(toggled && curScroll < threshold) {
      header.classList.remove('scrolled');
    }

    if(toggled) {
      prevDirection = curDirection;
    }
  };

  const toggleHeader = function() {
    toggled = true;

    if(curDirection === 2 && curScroll > threshold && !cart.classList.contains('xoo-wsc-cart-active')) {
      header.classList.add('hidden', 'scrolled');
    }

    else if (curDirection === 1) {
      header.classList.remove('hidden');
    }

    else {
      toggled = false;
    }

    return toggled;
  };

  window.addEventListener('scroll', checkScroll);

})
