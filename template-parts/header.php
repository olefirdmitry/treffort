<header class="ba-header">
	<div class="ba-header-bar">
		<?php foundationpress_top_bar_r(); ?>
		<a class="ba-header-logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>">
			<?php if(has_custom_logo()) : ?>
				<?php
				$custom_logo_id = get_theme_mod( 'custom_logo' );
				$image = wp_get_attachment_image_src( $custom_logo_id , 'full' );
				?>
				<img src="<?php echo $image[0] ?>" alt="<?php bloginfo( 'name' ); ?>" />
			<?php else : ?>
				<?php bloginfo( 'name' ); ?>
			<?php endif ?>
		</a>
		<ul class="ba-header__icons">
			<li class="hide-for-small-only">
				<a href="#">
					<svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
						<title>Mask</title>
						<g id="desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<g id="shirt" transform="translate(-1407.000000, -51.000000)" fill="#1C2B53" fill-rule="nonzero">
								<g id="Group" transform="translate(1407.000000, 49.000000)">
									<path d="M19.7,20.3 L15.2,15.8 C16.3,14.4 17,12.5 17,10.5 C17,5.8 13.2,2 8.5,2 C3.8,2 0,5.8 0,10.5 C0,15.2 3.8,19 8.5,19 C10.5,19 12.3,18.3 13.8,17.2 L18.3,21.7 C18.5,21.9 18.8,22 19,22 C19.2,22 19.5,21.9 19.7,21.7 C20.1,21.3 20.1,20.7 19.7,20.3 Z M2,10.5 C2,6.9 4.9,4 8.5,4 C12.1,4 15,6.9 15,10.5 C15,12.3 14.3,13.9 13.1,15.1 C13.1,15.1 13.1,15.1 13.1,15.1 C13.1,15.1 13.1,15.1 13.1,15.1 C11.9,16.3 10.3,17 8.5,17 C4.9,17 2,14.1 2,10.5 Z" id="Mask"></path>
								</g>
							</g>
						</g>
					</svg>
				</a>
			</li>
			<li>
				<a href="#">
					<svg width="18px" height="20px" viewBox="0 0 18 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
						<title>Mask</title>
						<g id="desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
							<g id="shirt" transform="translate(-1467.000000, -51.000000)" fill="#1C2B53" fill-rule="nonzero">
								<g id="Group" transform="translate(1407.000000, 49.000000)">
									<path d="M78,19 L78,21 C78,21.6 77.6,22 77,22 C76.4,22 76,21.6 76,21 L76,19 C76,17.3 74.7,16 73,16 L65,16 C63.3,16 62,17.3 62,19 L62,21 C62,21.6 61.6,22 61,22 C60.4,22 60,21.6 60,21 L60,19 C60,16.2 62.2,14 65,14 L73,14 C75.8,14 78,16.2 78,19 Z M64,7 C64,4.2 66.2,2 69,2 C71.8,2 74,4.2 74,7 C74,9.8 71.8,12 69,12 C66.2,12 64,9.8 64,7 Z M66,7 C66,8.7 67.3,10 69,10 C70.7,10 72,8.7 72,7 C72,5.3 70.7,4 69,4 C67.3,4 66,5.3 66,7 Z" id="Mask"></path>
								</g>
							</g>
						</g>
					</svg>
				</a>
			</li>
			<li data-cart-wrap>
				<div class="cart-count" data-cart-count="0"></div>
				<svg width="24px" height="23px" viewBox="0 0 24 23" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
					<title>Mask</title>
					<g id="desktop" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
						<g id="shirt" transform="translate(-1526.000000, -49.000000)" fill="#1C2B53" fill-rule="nonzero">
							<g id="Group" transform="translate(1407.000000, 49.000000)">
								<path d="M127.998368,23.0032691 C126.799347,23.0032691 126,22.2026153 126,21.0016345 C126,19.8006538 126.799347,19 127.998368,19 C129.197389,19 129.996736,19.8006538 129.996736,21.0016345 C129.996736,22.2026153 129.197389,23.0032691 127.998368,23.0032691 Z M138.998368,23 C137.799347,23 137,22.2 137,21 C137,19.8 137.799347,19 138.998368,19 C140.197389,19 140.996736,19.8 140.996736,21 C140.996736,22.2 140.197389,23 138.998368,23 Z M123.180316,2 L120,2 C119.447715,2 119,1.55228475 119,1 C119,0.44771525 119.447715,1.77635684e-15 120,1.77635684e-15 L124,1.77635684e-15 C124.476627,1.77635684e-15 124.887011,0.336385214 124.980553,0.80374304 L125.82043,5 L142,5 C142.627397,5 143.099826,5.57103711 142.982298,6.1873273 L141.38084,14.5848164 C141.097734,16.0101637 139.833764,17.02766 138.4,17 L128.69916,16.9998164 C127.246236,17.02766 125.982266,16.0101637 125.699447,14.586257 L124.028791,6.23920581 C124.022013,6.21159422 124.016382,6.18353331 124.011952,6.15507521 L123.180316,2 Z M126.220729,7 L127.66084,14.1951836 C127.755208,14.6702993 128.176532,15.0094648 128.68,15 L138.41916,15.0001836 C138.903468,15.0094648 139.324792,14.6702993 139.417702,14.2026727 L140.791275,7 L126.220729,7 Z" id="Mask"></path>
							</g>
						</g>
					</g>
				</svg>
			</li>
		</ul>
		<nav class="mobile-nav">
			<button aria-label="<?php _e( 'Main Menu', 'foundationpress' ); ?>" class="menu-icon hide-for-large" type="button" data-toggle="off-canvas-menu"></button>
		</nav>
	</div>
</header>
