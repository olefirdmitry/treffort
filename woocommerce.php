<?php

get_header();

$context            = Timber::context();
$context['sidebar'] = Timber::get_widgets( 'shop-sidebar' );

if ( is_singular( 'product' ) ) {
	$context['post']    = Timber::get_post();
	$product            = wc_get_product( $context['post']->ID );
	$context['product'] = $product;

	// Get related products
	$related_limit               = wc_get_loop_prop( 'columns' );
	$related_ids                 = wc_get_related_products( $context['post']->id, $related_limit );
	$context['related_products'] =  Timber::get_posts( $related_ids );
	$context['product_fields'] = get_field('product');
	$context['usp'] = get_field('usp', 'options');
	$context['cta'] = get_field('cta', 'options');

	// Restore the context and loop back to the main query loop.
	wp_reset_postdata();

	Timber::render( 'woo/single-product.twig', $context );
} else {
	$posts = Timber::get_posts();
	$context['products'] = $posts;
	$context['shop'] = get_field('shop', get_option( 'woocommerce_shop_page_id' ));
	$context['usp'] = get_field('usp', 'options');
	$context['cta'] = get_field('cta', 'options');

	if ( is_product_category() ) {
		$queried_object = get_queried_object();
		$term_id = $queried_object->term_id;
		$context['category'] = get_term( $term_id, 'product_cat' );
		$context['title'] = single_term_title( '', false );
	}

	Timber::render( 'woo/archive.twig', $context );
}

get_footer();
